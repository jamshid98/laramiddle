<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/user' , function (){
    return "this is simple user";
})->middleware('admin' , 'admin:false');
Route::get('/admin', function () {
    return "this is admin";
});

Route::get('/posts', 'PostController@index');
Route::get('/form/edit' , 'PostController@edit');

Route::get('/form/create', 'PostController@create');
Route::get('/getuser' , 'CocaCola@getUsers');
Route::resource('/categories' , 'CategoryController');
Route::resource('/news' , 'NewsController');
