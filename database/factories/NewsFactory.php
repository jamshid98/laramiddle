<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    return [
        'title' => $faker->words(10 , true),
        'body' => $faker->text,
        'category' => $faker->word,
        'seen' => $faker->numberBetween(1,2000)
    ];
});
