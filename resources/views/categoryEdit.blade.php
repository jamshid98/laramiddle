@extends('layouts.master')
@section('content')
    <form class="form-horizontal" action="{{route('categories.update' , ['for_up_cat' => $for_up_cat])}}" method="post">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label class="control-label col-sm-2" for="categoryName">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="categoryName" name="categoryName" required  value="{{$for_up_cat->name}}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" value="submit">
            </div>
        </div>
    </form>

@stop