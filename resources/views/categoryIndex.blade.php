@extends('layouts.master')
@section('content')

    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Category Name</th>
            <th >Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($categories as $category)
            <tr >
                <td>{{$category->id}}</td>
                <td>{{$category->name}}</td>
                <td class="d-flex">
                    <a href="{{route('categories.edit' , ['id' => $category->id])}}" class="btn btn-primary">Edit</a>
                    <form action="{{route('categories.destroy' , ['id' => $category->id])}}" method="post">
                        @method('DELETE')
                        @csrf
                        <input type="submit" class="btn btn-danger" value="delete">
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <br>
    <a href="{{route('categories.create')}}" class="btn btn-success">Create</a>
@stop